<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
	<title>Insert title here</title>
</head>
<body>
	<h1>DOANH SỐ THEO LOẠI</h1>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Loại</th>
				<th>Số lượng</th>
				<th>Doanh số</th>
				<th>Giá Min</th>
				<th>Giá Max</th>
				<th>Giá Avg</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="item" items="${data}">
			<tr>
				<td>${item[0]}</td>
				<td>${item[1]}</td>
				<td>${item[2]}</td>
				<td>${item[3]}</td>
				<td>${item[4]}</td>
				<td>${item[5]}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</body>
</html>