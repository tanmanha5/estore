<%@ page pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Insert title here</title>
</head>
<body>
	<h1>CHANGE PASSWORD</h1>
	<div class="panel panel-default">
		<div class="panel-heading">${message}</div>
		<div class="panel-body">
			<form action="/account/change" method="post">
				<div class="form-group">
					<label>Username:</label> 
					<input class="form-control" name="id">
				</div>
				<div class="form-group">
					<label>Password:</label> 
					<input type="password" class="form-control" name="password">
				</div>
				<div class="form-group">
					<label>New Password:</label> 
					<input type="password" class="form-control" name="password1">
				</div>
				<div class="form-group">
					<label>Confirm New Password:</label> 
					<input type="password" class="form-control" name="password2">
				</div>
				<button class="btn btn-primary">Change</button>
			</form>
		</div>
	</div>
</body>
</html>