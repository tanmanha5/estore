package com.estore.controller;
/**
 * Description:
 * <p>
 * Change history:
 * Date		            Defect#                 	Person                      comments
 * ---------------------------------------------------------------------------------------------------------------------
 * 6/8/2020           	******                  	Manh.Nguyen                 create file
 *
 * @author: Manh.Nguyen
 * @date: 6/8/2020 2:01 AM
 * @copyright: 2020, EStore. All Rights Reserved.
 */
import java.io.File;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.estore.dao.CustomerDAO;
import com.estore.entity.Customer;
import com.estore.service.CookieService;
import com.estore.service.MailerService;

@Controller
public class AccountController {
	@Autowired
	CustomerDAO dao;
	
	@Autowired
	MailerService mailer;
	
	@Autowired
	CookieService cookie;
	
	@Autowired
	HttpSession session;
	
	@Autowired
	ServletContext app;
	
	@Autowired
	HttpServletRequest request;
	
	/*
	 * LOGIN & LOGOFF
	 */
	@GetMapping("/account/login")
	public String login(Model model) {
		String[] user = cookie.getValue("user", "").split("~");
		if(user.length == 2) {
			model.addAttribute("username", user[0]);
			model.addAttribute("password", user[1]);
		}
		return "account/login";
	}
	@PostMapping("/account/login")
	public String login(Model model, 
			@RequestParam("id") String id,
			@RequestParam("password") String pw,
			@RequestParam(name="remember", defaultValue="false") Boolean rm) {
		Customer user = dao.findById(id);
		if(user == null) {
			model.addAttribute("message", "Invalid username!");
		}
		else if(!user.getPassword().equals(pw)) {
			model.addAttribute("message", "Invalid password!");
		}
		else if(!user.isActivated()) {
			model.addAttribute("message", "Your account is not activated!");
		}
		else {
			model.addAttribute("message", "Login successfully!");
			session.setAttribute("user", user);
			if(rm == false) {
				cookie.create("user", "", 0);
			}
			else {
				cookie.create("user", id+"~"+pw, 30);
			}
			
			String secureUrl = (String) session.getAttribute("secure-url");
			if(secureUrl != null) {
				return "redirect:"+secureUrl;
			}
		}
		
		return "account/login";
	}
	@RequestMapping("/account/logoff")
	public String logoff(Model model) {
		session.removeAttribute("user");
		return "redirect:/home/index";
	}
	
	/*
	 * REGISTER & ACTIVATE
	 */
	@GetMapping("/account/register")
	public String register(@ModelAttribute("user") Customer user) {
		return "account/register";
	}
	@PostMapping("/account/register")
	public String register(Model model, 
			@Validated @ModelAttribute("user") Customer user, 
			BindingResult errors, 
			@RequestParam("_photo") MultipartFile file) {
		user.setActivated(false);
		user.setAdmin(false);
		if(file.isEmpty()) {
			user.setPhoto("user.png");
		}
		else {
			String filename = file.getOriginalFilename();
			filename = UUID.randomUUID()+filename.substring(filename.lastIndexOf('.'));
			user.setPhoto(filename);
			String path = app.getRealPath("/static/images/customers/"+user.getPhoto());
			try {
				file.transferTo(new File(path));
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(errors.hasErrors()) {
			model.addAttribute("message", "Vui lòng sửa các lỗi sau");
		}
		else {
			dao.create(user);
			model.addAttribute("message", "Đăng ký thành công");
			
			String subject = "Welcome mail";
			String url = request.getRequestURL().toString().replace("register", "activate/"+user.getId());
			String body = "Xin chào<hr><a href='?'>Activate</a>".replace("?", url);
			mailer.send(user.getEmail(), subject, body);
		}
		return "account/register";
	}
	@RequestMapping("/account/activate/{id}")
	public String activate(@PathVariable("id") String id) {
		Customer user = dao.findById(id);
		user.setActivated(true);
		dao.update(user);
		return "redirect:/account/login";
	}
	
	/*
	 * FORGOT PASSWORD
	 */
	@GetMapping("/account/forgot")
	public String forgot(Model model) {
		return "account/forgot";
	}
	@PostMapping("/account/forgot")
	public String forgot(Model model, 
			@RequestParam("id") String id,
			@RequestParam("email") String email) {
		Customer user = dao.findById(id);
		if(user == null) {
			model.addAttribute("message", "Invalid username");
		}
		else if(!email.equals(user.getEmail())) {
			model.addAttribute("message", "Invalid email address");
		}
		else {
			model.addAttribute("message", "Your password was sent to your inbox");
			String subject = "Your password";
			String body = user.getPassword();
			mailer.send(user.getEmail(), subject, body);
		}
		return "account/forgot";
	}
	
	/*
	 * CHANGE PASSWORD
	 */
	@GetMapping("/account/change")
	public String change(Model model) {
		return "account/change";
	}
	@PostMapping("/account/change")
	public String change(Model model, 
			@RequestParam("id") String id,
			@RequestParam("password") String pw,
			@RequestParam("password1") String pw1,
			@RequestParam("password2") String pw2) {
		if(!pw1.equals(pw2)) {
			model.addAttribute("message", "Confirm password is invalid");
		}
		else {
			Customer user = dao.findById(id);
			if(user == null) {
				model.addAttribute("message", "Invalid username");
			}
			else if(!pw.equals(user.getPassword())) {
				model.addAttribute("message", "Invalid current password");
			}
			else {
				user.setPassword(pw1);
				dao.update(user);
				model.addAttribute("message", "Your password was changed!");
			}
		}
		return "account/change";
	}
	
	/*
	 * PROFILE EDITION
	 */
	@GetMapping("/account/edit")
	public String edit(Model model) {
		model.addAttribute("user", session.getAttribute("user"));
		return "account/edit";
	}
	@PostMapping("/account/edit")
	public String edit(Model model, 
			@Validated @ModelAttribute("user") Customer user, 
			BindingResult errors, 
			@RequestParam("_photo") MultipartFile file) {
		if(!file.isEmpty()) {
			String filename = file.getOriginalFilename();
			filename = UUID.randomUUID()+filename.substring(filename.lastIndexOf('.'));
			user.setPhoto(filename);
			String path = app.getRealPath("/static/images/customers/"+user.getPhoto());
			try {
				file.transferTo(new File(path));
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(errors.hasErrors()) {
			model.addAttribute("message", "Vui lòng sửa các lỗi sau");
		}
		else {
			dao.update(user);
			session.setAttribute("user", user);
			model.addAttribute("message", "Cập nhật thành công");
		}
		return "account/edit";
	}
}
