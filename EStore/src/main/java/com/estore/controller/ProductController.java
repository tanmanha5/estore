package com.estore.controller;
/**
 * Description:
 * <p>
 * Change history:
 * Date		            Defect#                 	Person                      comments
 * ---------------------------------------------------------------------------------------------------------------------
 * 6/8/2020           	******                  	Manh.Nguyen                 create file
 *
 * @author: Manh.Nguyen
 * @date: 6/8/2020 2:01 AM
 * @copyright: 2020, EStore. All Rights Reserved.
 */
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.estore.dao.CategoryDAO;
import com.estore.dao.ProductDAO;
import com.estore.entity.Category;
import com.estore.entity.Product;
import com.estore.service.CookieService;
import com.estore.service.MailerService;

@Controller
public class ProductController {
	@Autowired
	CategoryDAO cdao;
	@Autowired
	ProductDAO pdao;
	
	@Autowired
	CookieService cookie;
	
	@Autowired
	MailerService mailer;
	
	@Autowired
	HttpServletRequest request;
	
	@ResponseBody
	@RequestMapping("/prod/send-to-friend/{id}")
	public String sendToFriend(
			@PathVariable("id") Integer id,
			@RequestParam("from") String from,
			@RequestParam("to") String to,
			@RequestParam("subject") String subject,
			@RequestParam("content") String content) {
		String url = request.getRequestURL().toString().replace("send-to-friend", "detail");
		content += "<hr><a href='?'>Thông tin hàng hóa</a>".replace("?", url);

		mailer.send(to, subject, content, from);
		return "Thông tin hàng hóa đã được gửi đến bạn bè";
	}
	
	@ResponseBody
	@RequestMapping("/prod/mark-as-favorite/{id}")
	public String markAsFavorite(@PathVariable("id") Integer id) {
		String favorites = cookie.getValue("favorites", id.toString());
		if(!favorites.contains(id.toString())) {
			favorites += "," + id;
		}
		cookie.create("favorites", favorites, 30);
		return "Đã ghi nhận mặt hàng";
	}
	@RequestMapping("/prod/list-by-spec/{id}")
	public String listBySpecials(Model model, @PathVariable("id") Integer id) {
		List<Product> products = pdao.findBySpecials(id);
		model.addAttribute("prods", products);
		return "product/list";
	}
	
	@RequestMapping("/prod/list-by-keywords")
	public String listByKeywords(Model model, @RequestParam("keywords") String keywords) {
		List<Product> products = pdao.findByKeywords(keywords);
		model.addAttribute("prods", products);
		return "product/list";
	}
	
	@RequestMapping("/prod/list-by-cate/{id}")
	public String listByCategory(Model model, @PathVariable("id") Integer id) {
		Category category = cdao.findById(id);
		model.addAttribute("prods", category.getProducts());
		return "product/list";
	}
	
	@RequestMapping("/prod/detail/{id}")
	public String detail(Model model, @PathVariable("id") Integer id) {
		Product product = pdao.findById(id);
		
		product.setViewCount(product.getViewCount() + 1);
		pdao.update(product);
		
		model.addAttribute("prod", product);
		return "product/detail";
	}
}
