package com.estore.controller;
/**
 * Description:
 * <p>
 * Change history:
 * Date		            Defect#                 	Person                      comments
 * ---------------------------------------------------------------------------------------------------------------------
 * 6/8/2020           	******                  	Manh.Nguyen                 create file
 *
 * @author: Manh.Nguyen
 * @date: 6/8/2020 2:01 AM
 * @copyright: 2020, EStore. All Rights Reserved.
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.estore.dao.ReportDAO;

@Controller
public class AdminReportController {
	@Autowired
	ReportDAO dao;
	
	@RequestMapping("/admin/report/revenue-by-quarter")
	public String revenueByCategoryQuarter(Model model) {
		List<Object[]> data = dao.revenueByQuarter();
		model.addAttribute("data", data);
		return "admin/report/revenue-by-quarter";
	}
	
	@RequestMapping("/admin/report/revenue-by-category")
	public String revenueByCategory(Model model) {
		List<Object[]> data = dao.revenueByCategory();
		model.addAttribute("data", data);
		return "admin/report/revenue-by-category";
	}
	
	@RequestMapping("/admin/report/inventory")
	public String inventory(Model model) {
		List<Object[]> data = dao.inventoryByCategory();
		model.addAttribute("data", data);
		return "admin/report/inventory";
	}
	
}
