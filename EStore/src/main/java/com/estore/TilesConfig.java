package com.estore;
/**
 * Description:
 * <p>
 * Change history:
 * Date		            Defect#                 	Person                      comments
 * ---------------------------------------------------------------------------------------------------------------------
 * 6/8/2020           	******                  	Manh.Nguyen                 create file
 *
 * @author: Manh.Nguyen
 * @date: 6/8/2020 2:01 AM
 * @copyright: 2020, EStore. All Rights Reserved.
 */
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

@Configuration
public class TilesConfig {
	@Bean("viewResolver")
	public ViewResolver getViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setViewClass(TilesView.class);
		return resolver;
	}
	@Bean("tilesConfigurer")
	public TilesConfigurer getTilesConfigurer() {
		TilesConfigurer configurer = new TilesConfigurer();
		configurer.setDefinitions("/WEB-INF/tiles.xml");
		return configurer;
	}
}
