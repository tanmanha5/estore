package com.estore.dao;
/**
 * Description:
 * <p>
 * Change history:
 * Date		            Defect#                 	Person                      comments
 * ---------------------------------------------------------------------------------------------------------------------
 * 6/8/2020           	******                  	Manh.Nguyen                 create file
 *
 * @author: Manh.Nguyen
 * @date: 6/8/2020 2:01 AM
 * @copyright: 2020, EStore. All Rights Reserved.
 */
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class ReportDAO {
	@Autowired
	SessionFactory factory;

	public List<Object[]> inventoryByCategory() {
		Session session = factory.getCurrentSession();
		String hql = "SELECT p.category.nameVN,"
				+ " sum(p.quantity),  "
				+ " sum(p.quantity*p.unitPrice), "
				+ " min(p.unitPrice), "
				+ " max(p.unitPrice), "
				+ " avg(p.unitPrice)"
				+ " FROM Product p"
				+ " GROUP BY p.category.nameVN";
		TypedQuery<Object[]> query = session.createQuery(hql, Object[].class);
		List<Object[]> list = query.getResultList();
		return list;
	}

	public List<Object[]> revenueByCategory() {
		Session session = factory.getCurrentSession();
		String hql = "SELECT d.product.category.nameVN,"
				+ " sum(d.quantity),  "
				+ " sum(d.quantity*d.unitPrice*(1-d.discount)), "
				+ " min(d.unitPrice), "
				+ " max(d.unitPrice), "
				+ " avg(d.unitPrice)"
				+ " FROM OrderDetail d"
				+ " GROUP BY d.product.category.nameVN";
		TypedQuery<Object[]> query = session.createQuery(hql, Object[].class);
		List<Object[]> list = query.getResultList();
		return list;
	}
	
	public List<Object[]> revenueByQuarter() {
		Session session = factory.getCurrentSession();
		String hql = "SELECT ceiling(month(d.order.orderDate)/3.0),"
				+ " sum(d.quantity),  "
				+ " sum(d.quantity*d.unitPrice*(1-d.discount)), "
				+ " min(d.unitPrice), "
				+ " max(d.unitPrice), "
				+ " avg(d.unitPrice)"
				+ " FROM OrderDetail d"
				+ " GROUP BY ceiling(month(d.order.orderDate)/3.0)"
				+ " ORDER BY ceiling(month(d.order.orderDate)/3.0)";
		TypedQuery<Object[]> query = session.createQuery(hql, Object[].class);
		List<Object[]> list = query.getResultList();
		return list;
	}
}
