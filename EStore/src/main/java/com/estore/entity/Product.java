package com.estore.entity;
/**
 * Description:
 * <p>
 * Change history:
 * Date		            Defect#                 	Person                      comments
 * ---------------------------------------------------------------------------------------------------------------------
 * 6/8/2020           	******                  	Manh.Nguyen                 create file
 *
 * @author: Manh.Nguyen
 * @date: 6/8/2020 2:01 AM
 * @copyright: 2020, EStore. All Rights Reserved.
 */
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="Products")
@Getter
@Setter
public class Product  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private double unitPrice;
	private String image;
	@Temporal(TemporalType.DATE)
	private Date productDate;
	private boolean available;
	private int quantity;
	private String description;
	private double discount;
	private int viewCount;
	private boolean special;
	
	@ManyToOne()
	@JoinColumn(name="CategoryId")
	private Category category;
	
	@JsonIgnore
	@OneToMany(mappedBy="product")
	private List<OrderDetail> orderDetails;

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}
}
